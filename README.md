# SoftwareCourseDesign

#### 介绍
这是一块基于MUI框架的app，理论上具有Android和IOS跨平台功能。

后端基于springboot框架，使用redis和mysql数据库，nginx负载均衡，docker镜像部署在服务器上。

#### 软件架构
软件总体分为前端和后端项目

#### 前端项目提交日志

#### 后端项目提交日志

1.第一次提交，主要确定后端项目的框架模板。


#### 安装教程

1.  根据文档中用户手册方式安装apk

#### 使用说明

1.  根据文档中用户手册内容使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
