/*!
 * ======================================================
 * FeedBack Template For MUI (http://dev.dcloud.net.cn/mui)
 * =======================================================
 * @version:1.0.0
 * @author:cuihongbao@dcloud.io
 */
var img_src = "";
var tasktype="";
var changeid="";
(function() {
	
	var feedback = {
		title: document.getElementById('tiezititle'), 
		imageList: document.getElementById('userPhoto'),
		content: document.getElementById("textarea"),
		submitBtn: document.getElementById('submit'),
		money: document.getElementById('money'),
		position: document.getElementById("position")
	};
	feedback.files = [];
	feedback.uploader = null;  
	feedback.deviceInfo = null; 
	mui.plusReady(function() {
		//设备信息，无需修改
		feedback.deviceInfo = {
			appid: plus.runtime.appid, 
			imei: plus.device.imei, //设备标识
			images: feedback.files, //图片文件
			p: mui.os.android ? 'a' : 'i', //平台类型，i表示iOS平台，a表示Android平台。
			md: plus.device.model, //设备型号
			app_version: plus.runtime.version,
			plus_version: plus.runtime.innerVersion, //基座版本号
			os:  mui.os.version,
			net: ''+plus.networkinfo.getCurrentType()
		}
	});
	/**
	 *提交成功之后，恢复表单项 
	 */
	feedback.clearForm = function() {
		feedback.title.value = '';
		feedback.content.value = '';
		feedback.imageList.innerHTML = '';
		feedback.money.innerHTML="";
		feedback.type.innerHTML='帖子类型<span class="mui-icon mui-icon-arrowdown"></span>';
		feedback.newPlaceholder();
		feedback.files = [];
		index = 0;
		size = 0;
		imageIndexIdNum = 0;
		starIndex = 0;
		//清除所有星标
		
	};
	
	
	feedback.submitBtn.addEventListener('tap', function(event) {
		
		feedback.title=document.getElementById("tiezititle");
		feedback.content=document.getElementById("textarea");		 
		if (feedback.title.value == '' || feedback.content.value == '' ) {
			return mui.toast('标题或帖子内容不能为空呦~~~');
		}
		if (feedback.title.value.length > 20|| feedback.content.value.length > 500) {
			return mui.toast('标题或帖子信息超长,请重新填写~');
		}
		//判断网络连接
		if(plus.networkinfo.getCurrentType()==plus.networkinfo.CONNECTION_NONE){
			return mui.toast("连接网络失败，请稍后再试");
		}
		
		if (tasktype == "wait") {
			
			feedback.money=document.getElementById("money");
			if(feedback.money.value==""){
				return mui.toast("金额不能为空")
			}
			feedback.position=document.getElementById("position");
			if(feedback.position.value==""){
				return mui.toast("预计收货地址不能为空")
			}
			console.log(changeid);
			var param = {
				id:changeid,
				title: feedback.title.value,
				status: tasktype,
				img: img_src,
				value:feedback.content.value,
				money:feedback.money.value,
				position:feedback.position.value	
			};
			//console.log(param);
		}
		else {
			feedback.position=document.getElementById("position");
			if(feedback.position.value==undefined){
				var param = {
					id:changeid,
					title: feedback.title.value,
					status: tasktype,
					img: img_src,
					value:feedback.content.value,
					money:0,
					position:""
				};
			}
			else{
				var param = {
					id:changeid,
					title: feedback.title.value,
					status: tasktype,
					img: img_src,
					value:feedback.content.value,
					money:0,
					position:feedback.position.innerHTML
				};
			}
			
		}
		
		var photo = document.getElementById('userPhoto');
		//if (photo.innerHTML === '已上传')
		
		
		if(photo.src[0]!="h"){
			sendPhoto(photo.src, param);
		}
		else{
			feedback.send(param);
		}
		
		
		//console.log(img_src);
		
		//feedback.send(param);
		//console.log("发送");
		
	}, false);
	
	function sendPhoto(photoPath, param) {
		var ret;
		var server = "https://gsh-images.oss-cn-beijing.aliyuncs.com/";  //申请到的阿里云OSS地址
		var OSSAccessKeyId= 'LTAI5tFjHyDKjdu7BvadtRSd';  //申请到的阿里云AccessKeyId和AccessKeySecret
		var AccessKeySecret= 'Kb4eljkzgzsP02ASZiZkEYl6AmDS19';//需要用自己申请的进行替换
		var files = [];  //存储文件信息的数组
		var fname = "xxx.jpg";  //表示文件名，例如  XXXX.jpg;
		var dir ="img/";  //指定上传目录，此处指定上传到app目录下
		
		var testName;   //本地测试用的
		/*
		* 阿里云参数设置，用于计算签名signature
		*/
		var policyText = {
			"expiration": "2022-10-01T12:00:00.000Z", //设置该Policy的失效时间，超过这个失效时间之后，就没有办法通过这个policy上传文件了
			"conditions": [
				["content-length-range", 0, 1048576000] // 设置上传文件的大小限制
			]
		};
		var policyBase64 = Base64.encode(JSON.stringify(policyText));
		var message = policyBase64;
		var bytes = Crypto.HMAC(Crypto.SHA1, message, AccessKeySecret, {
			asBytes: true
		});
		var signature = Crypto.util.bytesToBase64(bytes);
		
		
		
		
		//		var g_object_name = 'XXXXX';
		//		var new_multipart_params = {
		//			'key': 'g_object_name',
		//			'policy': policyBase64,
		//			'OSSAccessKeyId': OSSAccessKeyId,
		//			'success_action_status': '200', //让服务端返回200,不然，默认会返回204
		//			'signature': signature,
		//		};
		
		// 上传文件
		//function upload() {
			// if(files.length <= 0) {
			// 	plus.nativeUI.alert("没有添加上传文件！");
			// 	return;
			// }
			mui.toast("开始上传");
			var wt = plus.nativeUI.showWaiting();
			var task = plus.uploader.createUpload(server, {method: "POST"},
			function(t, status) { //上传完成
				//console.log("t" + JSON.stringify(t));
				if(status == 200) {
					// mui.toast("上传成功：" + t.responseText);
					//至此上传成功，上传后的图片完整地址为server+testName
					var uploaderFileObj = {
						"server":  server,
						"path":  keyname
					};
					//console.log(JSON.stringify(uploaderFileObj));
					ret =  uploaderFileObj.server + uploaderFileObj.path;
					img_src = uploaderFileObj.server + uploaderFileObj.path;
					//console.log(img_src);
					param.img = img_src;
					//console.log("param.img = " + param.img);
					console.log("hhhh");
					feedback.send(param);
					// console.log("photo.src"+photo.src);
					
					//window.open("./my.html");
					// plus.storage.setItem("uploader", JSON.stringify(uploaderFileObj));
					wt.close();
					
					// var w = plus.webview.create("my.html", "my.html", {
					// 	scrollIndicator: 'none',
					// 	scalable: false
					// });
					// w.addEventListener("loaded", function() {
					// 	
					// 	w.show("slide-in-right", 300);
					// }, false);
				} else {
					// mui.toast("上传失败：" + status);
						wt.close();
				}
			});
			
			var suffix1 = get_suffix(fname);  //文件后缀  例如   .jpg
			var keyname = dir + new Date().getTime() + ".jpg";
					
			testName = keyname;
			console.log("keyname" + keyname);
					
					
			//按照之前说明的参数类型，按顺序添加参数
			task.addData("key", keyname);
			task.addData("policy", policyBase64);
			task.addData("OSSAccessKeyId", OSSAccessKeyId);
			task.addData("success_action_status", "200");
			task.addData("signature", signature);
			//console.log("files[0]" + JSON.stringify(files[0]));
			console.log(photoPath);
			task.addFile(photoPath, {
				key: "file",
				name: "file",
				mime: "image/jpeg"
			});
			
			// var f = files[0];
			// task.addFile(f.path, {
			// 	key: "file",
			// 	name: "file",
			// 	mime: "image/jpeg"
			// });
			
			task.start();
			
		//}
					
		//得到文件名的后缀
		function get_suffix(filename) {
			var pos = filename.lastIndexOf('.');
			var suffix = '';
			if(pos != -1) {
				suffix = filename.substring(pos)
			}
			return suffix;
		}

		// //添加文件
		// var index = 1;
		
		// function appendFile(p) {
		// 	//var fe = document.getElementById("files");
		// 	//var li = document.createElement("li");
		// 	var n = p.substr(p.lastIndexOf('/') + 1);
		// 	fname = n;
		// 	li.innerText = n;
		// 	//fe.appendChild(li);
		// 	files.push({
		// 		name: "uploadkey" + index,
		// 		path: p
		// 	});
		// 	index++;
		// 	empty.style.display = "none";
		// }
	}
	
	
	feedback.send = function(tieze) {
		console.log("hhhh");
		mui.ajax("http://81.69.56.43:8080/modifytask",{
		 data:tieze,
		 type:'post',
		 timeout:10000,
		 success:function(data){
			//对回调结果进行处理
			
			
			// taskList = JSON.parse(data);
			// for (let i = 0; i < taskList.length; i++) {
			// 	addOneTask(taskList[i]);
			// }
			if(data=="modify_success"){
				mui.toast("修改成功");
				setTimeout(function(){
					window.open("./index.html");
				},1000);
			}
			else if(data=="modify_fail"){
				mui.toast("修改失败");
			}
			else{
				mui.toast("不可修改");
			}
			
		},
		 
		 error:function(XMLHttpRequest,textStatus,errorThrown){
						//mui(".mui-btn-green").button("reset");
						if(textStatus==='timeout'){
					mui.toast("连接超时");
				}
				else if(textStatus==='error'){
					mui.toast("连接错误");
				}
		 }
		 });
		

// 		feedback.uploader = plus.uploader.createUpload(url, {
// 			method: 'POST'
// 		}, function(upload, status) {
// //			plus.nativeUI.closeWaiting()
// 			console.log("upload cb:"+upload.responseText);
// 			if(status==200){
// 				var data = JSON.parse(upload.responseText);
// 				//上传成功，重置表单
// 				if (data.ret === 0 && data.desc === 'Success') {
// 					mui.toast('反馈成功~')
// 					console.log("upload success");
// //					feedback.clearForm();
// 				}
// 			}else{
// 				console.log("upload fail");
// 			}
			
// 		});
		
// 		//添加上传数据
// 		mui.each(content, function(index, element) {
// 			if (index !== 'images') {
// 				console.log("addData:"+index+","+element);
// //				console.log(index);
// 				feedback.uploader.addData(index, element)
// 			} 
// 		});
// 		//添加上传文件
// 		mui.each(feedback.files, function(index, element) {
// 			var f = feedback.files[index];
// 			console.log("addFile:"+JSON.stringify(f));
// 			feedback.uploader.addFile(f.path, {
// 				key: f.name
// 			});
// 		});
// 		//开始上传任务
// 		feedback.uploader.start();
// 		mui.alert("发帖成功，点击确定关闭","发布帖子","确定",function () {
// 			feedback.clearForm();
// 			mui.back();
// 		});
//		plus.nativeUI.showWaiting();
	};
})();
	
	
	
	
  
 